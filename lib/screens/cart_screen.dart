import 'package:flutter/material.dart';
import "package:flutter_bloc/flutter_bloc.dart";
import 'package:flutter_cart/widgets/cart/cart_list.dart';

import '../blocs/cart/cart_bloc.dart';
import '../blocs/cart/cart_state.dart';
import '../widgets/views/error_view.dart';
import '../widgets/views/loading_view.dart';

class CartScreen extends StatelessWidget {
  static const routeName = '/cart';
  const CartScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Cart"),
        ),
        body: BlocBuilder<CartBloc, CartState>(builder: (context, state) {
          if (state is CartLoading) return const LoadingView();
          if (state is CartError) return ErrorView(message: state.message);
          if (state is CartLoaded) return CartList(cartItems: state.carts);
          return const SizedBox();
        }),
      ),
    );
  }
}
