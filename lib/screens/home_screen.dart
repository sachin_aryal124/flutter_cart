import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/product/product_bloc.dart';
import '../blocs/product/product_state.dart';
import '../widgets/cart/cart_quantity_badge.dart';
import '../widgets/product/product_list.dart';
import '../widgets/product_category/product_category_view.dart';
import '../widgets/views/error_view.dart';
import '../widgets/views/loading_view.dart';

class HomeScreen extends StatelessWidget {
  static const routeName = '/';
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Home"),
          actions: const [
            CartQuantityBadge(),
          ],
        ),
        body: Column(
          children: [
            const ProductCategoryView(),
            BlocBuilder<ProductBloc, ProductState>(
              builder: (context, state) {
                if (state is ProductLoading) return const ExpandedLoadingView();
                if (state is ProductError) {
                  return ExpandedErrorView(message: state.message);
                }
                if (state is ProductLoaded) {
                  return Expanded(
                    child: ProductList(
                      products: state.products,
                    ),
                  );
                }
                return const SizedBox();
              },
            ),
          ],
        ),
      ),
    );
  }
}
