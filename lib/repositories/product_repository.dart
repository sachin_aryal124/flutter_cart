import 'package:dio/dio.dart';

import '../models/product/product.dart';
import '../models/product_category/product_category.dart';
import '../utils/dio.dart';

class ProductRepository {
  final Map<String, List<Product>> _productsCache = {};

  Future<List<Product>> fetchProducts({
    String? productCategory,
  }) async {
    String cacheKey = productCategory ?? 'all-products';
    if (_productsCache.containsKey(cacheKey)) return _productsCache[cacheKey]!;
    String url = productCategory != null
        ? '/products/category/$productCategory'
        : '/products';
    Response response = await dio.get(url);
    List<dynamic> data = response.data;
    List<Product> products =
        data.map((item) => Product.fromJson(item)).toList();
    _productsCache[cacheKey] = products;
    return products;
  }

  Future<List<ProductCategory>> fetchProductCategory() async {
    Response response = await dio.get('/products/categories');
    List<dynamic> data = response.data;
    List<ProductCategory> productCategories = data
        .map(
          (item) => ProductCategory(name: item as String),
        )
        .toList();
    return productCategories;
  }
}
