import 'dart:convert';

import 'package:flutter_cart/blocs/cart/cart.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/cart/cart.dart';

const cartStorageKey = 'flutter_cart_app_cart_items';

class CartRepository {
  Future<List<Cart>> fetchCartItems() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> cartItemsString = prefs.getStringList(cartStorageKey) ?? [];
    List<Cart> cartItems = cartItemsString
        .map((cartItemString) => Cart.fromJson(json.decode(cartItemString)))
        .toList();
    return cartItems;
  }

  Future<void> saveCartItems(List<Cart> cartItems) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> cartItemsString =
        cartItems.map((cartItem) => json.encode(cartItem.toJson())).toList();
    prefs.setStringList(cartStorageKey, cartItemsString);
  }

  Future<List<Cart>> addToCart(Cart cart) async {
    List<Cart> existingCartItems = await fetchCartItems();
    List<Cart> updatedCartItems = existingCartItems
        .map((cartItem) => cartItem.product.id == cart.product.id
            ? cartItem.copyWith(
                quantity: cartItem.quantity + cart.quantity,
              )
            : cartItem)
        .toList();
    bool isProductInCart = existingCartItems
        .any((cartItem) => cartItem.product.id == cart.product.id);
    List<Cart> newCartItems =
        isProductInCart ? updatedCartItems : [...existingCartItems, cart];
    await saveCartItems(newCartItems);
    return newCartItems;
  }

  Future<List<Cart>> updateCartQuantity(
    Cart cart,
    UpdateCartQuantityAction action,
  ) async {
    List<Cart> existingCartItems = await fetchCartItems();
    List<Cart> updatedCartItems = existingCartItems
        .map((cartItem) => cartItem.id == cart.id
            ? cartItem.copyWith(
                quantity: action == UpdateCartQuantityAction.increment
                    ? cartItem.quantity + 1
                    : cartItem.quantity - 1,
              )
            : cartItem)
        .toList();
    List<Cart> updatedCartItemsWithoutZeroQuantity =
        updatedCartItems.where((cartItem) => cartItem.quantity > 0).toList();
    await saveCartItems(updatedCartItemsWithoutZeroQuantity);
    return updatedCartItemsWithoutZeroQuantity;
  }
}
