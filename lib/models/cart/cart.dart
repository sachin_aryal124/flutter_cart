// ignore: depend_on_referenced_packages
import 'package:json_annotation/json_annotation.dart';

import '../product/product.dart';

part 'cart.g.dart';

@JsonSerializable()
class Cart {
  final String id;
  final Product product;
  final int quantity;

  Cart({
    required this.id,
    required this.product,
    this.quantity = 1,
  });

  factory Cart.fromJson(Map<String, dynamic> json) => _$CartFromJson(json);
  Map<String, dynamic> toJson() => _$CartToJson(this);

  Cart copyWith({
    String? id,
    Product? product,
    int? quantity,
  }) {
    return Cart(
      id: id ?? this.id,
      product: product ?? this.product,
      quantity: quantity ?? this.quantity,
    );
  }
}
