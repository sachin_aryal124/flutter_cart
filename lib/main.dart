import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'blocs/cart/cart_bloc.dart';
import 'blocs/cart/cart_event.dart';
import 'blocs/product/product.dart';
import 'blocs/product_category/product_category_bloc.dart';
import 'blocs/product_category/product_category_event.dart';
import 'repositories/cart_repository.dart';
import 'repositories/product_repository.dart';
import 'screens/cart_screen.dart';
import 'screens/home_screen.dart';

void main() {
  runApp(
    MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => ProductRepository(),
        ),
        RepositoryProvider(
          create: (context) => CartRepository(),
        )
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => ProductBloc(
              productRepository: RepositoryProvider.of<ProductRepository>(
                context,
              ),
            )..add(LoadProduct()),
          ),
          BlocProvider(
            create: (context) => CartBloc(
              cartRepository: RepositoryProvider.of<CartRepository>(context),
            )..add(LoadCart()),
          ),
          BlocProvider(
            create: (context) => ProductCategoryBloc(
              productRepository: RepositoryProvider.of<ProductRepository>(
                context,
              ),
            )..add(
                LoadProductCategory(),
              ),
          ),
        ],
        child: const MyApp(),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.amber,
        primarySwatch: Colors.amber,
        appBarTheme: const AppBarTheme(
          centerTitle: true,
        ),
      ),
      routes: {
        HomeScreen.routeName: (context) => const HomeScreen(),
        CartScreen.routeName: (context) => const CartScreen(),
      },
    );
  }
}
