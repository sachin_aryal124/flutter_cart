import 'package:flutter_bloc/flutter_bloc.dart';

import '../../models/product/product.dart';
import '../../repositories/product_repository.dart';
import 'product.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  final ProductRepository productRepository;
  ProductBloc({
    required this.productRepository,
  }) : super(ProductInitial()) {
    on<LoadProduct>(_loadProducts);
    on<FilterProductByCategory>(_filterProductByCategory);
  }
  Future<void> _loadProducts(
    LoadProduct event,
    Emitter<ProductState> emit,
  ) async {
    emit(ProductLoading());
    try {
      List<Product> products = await productRepository.fetchProducts();
      emit(ProductLoaded(products: products));
    } catch (error) {
      emit(
        ProductError(
          message: error.toString(),
        ),
      );
    }
  }

  Future<void> _filterProductByCategory(
    FilterProductByCategory action,
    Emitter<ProductState> emit,
  ) async {
    emit(ProductLoading());
    try {
      List<Product> products = await productRepository.fetchProducts(
        productCategory: action.productCategory,
      );
      emit(ProductLoaded(products: products));
    } catch (e) {
      emit(ProductError(message: e.toString()));
    }
  }
}
