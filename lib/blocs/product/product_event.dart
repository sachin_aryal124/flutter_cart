import 'package:equatable/equatable.dart';

class ProductEvent extends Equatable {
  const ProductEvent();
  @override
  List<Object?> get props => [];
}

class LoadProduct extends ProductEvent {}

class FilterProductByCategory extends ProductEvent {
  final String productCategory;
  const FilterProductByCategory({
    required this.productCategory,
  });
  @override
  List<Object?> get props => [productCategory];
}
