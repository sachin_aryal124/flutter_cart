import 'package:equatable/equatable.dart';

import '../../models/product_category/product_category.dart';

class ProductCategoryState extends Equatable {
  const ProductCategoryState();
  @override
  List<Object?> get props => [];
}

class ProductCategoryInitial extends ProductCategoryState {}

class ProductCategoryLoading extends ProductCategoryState {}

class ProductCategoryLoaded extends ProductCategoryState {
  final List<ProductCategory> productCategories;

  const ProductCategoryLoaded({this.productCategories = const []});
  @override
  List<Object?> get props => [productCategories];
}

class ProductCategoryError extends ProductCategoryState {
  final String message;

  const ProductCategoryError({
    required this.message,
  });
  @override
  List<Object?> get props => [message];
}
