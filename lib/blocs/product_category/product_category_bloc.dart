import 'package:flutter_bloc/flutter_bloc.dart';

import '../../models/product_category/product_category.dart';
import '../../repositories/product_repository.dart';
import 'product_category_event.dart';
import 'product_category_state.dart';

class ProductCategoryBloc
    extends Bloc<ProductCategoryEvent, ProductCategoryState> {
  final ProductRepository productRepository;
  ProductCategoryBloc({
    required this.productRepository,
  }) : super(ProductCategoryInitial()) {
    on<LoadProductCategory>(_loadProductCategory);
  }
  Future<void> _loadProductCategory(
    LoadProductCategory action,
    Emitter<ProductCategoryState> emit,
  ) async {
    emit(ProductCategoryLoading());
    try {
      List<ProductCategory> productCategories =
          await productRepository.fetchProductCategory();
      emit(
        ProductCategoryLoaded(
          productCategories: productCategories,
        ),
      );
    } catch (e) {
      emit(
        ProductCategoryError(
          message: e.toString(),
        ),
      );
    }
  }
}
