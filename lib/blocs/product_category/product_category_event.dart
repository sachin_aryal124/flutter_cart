import 'package:equatable/equatable.dart';

class ProductCategoryEvent extends Equatable {
  const ProductCategoryEvent();
  @override
  List<Object?> get props => [];
}

class LoadProductCategory extends ProductCategoryEvent {}
