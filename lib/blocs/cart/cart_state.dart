import 'package:equatable/equatable.dart';

import '../../models/cart/cart.dart';

class CartState extends Equatable {
  const CartState();
  @override
  List<Object?> get props => [];
}

class CartInitial extends CartState {}

class CartLoading extends CartState {}

class CartLoaded extends CartState {
  final List<Cart> carts;
  const CartLoaded({this.carts = const []});

  @override
  List<Object?> get props => [carts];
}

class CartError extends CartState {
  final String message;
  const CartError({
    required this.message,
  });
  @override
  List<Object?> get props => [];
}
