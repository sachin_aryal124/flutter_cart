import 'package:equatable/equatable.dart';

import '../../models/cart/cart.dart';

enum UpdateCartQuantityAction { increment, decrement }

class CartEvent extends Equatable {
  const CartEvent();
  @override
  List<Object?> get props => [];
}

class LoadCart extends CartEvent {}

class AddToCart extends CartEvent {
  final Cart cart;
  const AddToCart({required this.cart});

  @override
  List<Object?> get props => [cart];
}

class UpdateCartQuantity extends CartEvent {
  final Cart cart;
  final UpdateCartQuantityAction action;

  const UpdateCartQuantity({
    required this.cart,
    required this.action,
  });

  @override
  List<Object?> get props => [cart, action];
}

class RemoveFromCart extends CartEvent {
  final String id;
  const RemoveFromCart({
    required this.id,
  });

  @override
  List<Object?> get props => [id];
}
