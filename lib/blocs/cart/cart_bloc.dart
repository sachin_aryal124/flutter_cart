import 'package:flutter_bloc/flutter_bloc.dart';

import '../../models/cart/cart.dart';
import '../../repositories/cart_repository.dart';
import 'cart_event.dart';
import 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  final CartRepository cartRepository;
  CartBloc({
    required this.cartRepository,
  }) : super(CartInitial()) {
    on<LoadCart>(_loadCart);
    on<AddToCart>(_addToCart);
    on<UpdateCartQuantity>(_updateCartQuantity);
  }
  Future<void> _loadCart(
    LoadCart event,
    Emitter<CartState> emit,
  ) async {
    emit(CartLoading());
    try {
      List<Cart> cartItems = await cartRepository.fetchCartItems();
      emit(CartLoaded(carts: cartItems));
    } catch (e) {
      emit(CartError(message: e.toString()));
    }
  }

  Future<void> _addToCart(
    AddToCart event,
    Emitter<CartState> emit,
  ) async {
    try {
      List<Cart> cartItems = await cartRepository.addToCart(event.cart);
      emit(CartLoaded(carts: cartItems));
    } catch (e) {
      emit(CartError(message: e.toString()));
    }
  }

  Future<void> _updateCartQuantity(
    UpdateCartQuantity action,
    Emitter<CartState> emit,
  ) async {
    try {
      List<Cart> updatedCartItems = await cartRepository.updateCartQuantity(
        action.cart,
        action.action,
      );
      emit(CartLoaded(carts: updatedCartItems));
    } catch (e) {
      emit(CartError(message: e.toString()));
    }
  }
}
