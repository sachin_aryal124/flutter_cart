String formatCurrency(double amount) => '\$${amount.toStringAsFixed(2)}';
