import 'package:dio/dio.dart';

BaseOptions baseOptions = BaseOptions(baseUrl: 'https://fakestoreapi.com');

Dio dio = Dio(baseOptions);
