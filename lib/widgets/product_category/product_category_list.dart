import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/product/product.dart';
import '../../models/product_category/product_category.dart';

class ProductCategoryList extends StatefulWidget {
  final List<ProductCategory> productCategories;
  const ProductCategoryList({
    super.key,
    required this.productCategories,
  });

  @override
  State<ProductCategoryList> createState() => _ProductCategoryListState();
}

class _ProductCategoryListState extends State<ProductCategoryList> {
  String? selectedCategory;
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => const SizedBox(width: 10),
      scrollDirection: Axis.horizontal,
      itemCount: widget.productCategories.length,
      itemBuilder: (context, index) {
        ProductCategory productCategory = widget.productCategories[index];
        return ChoiceChip(
          label: Text(productCategory.name),
          selected: productCategory.name == selectedCategory,
          onSelected: (_) {
            setState(() {
              selectedCategory = productCategory.name == selectedCategory
                  ? null
                  : productCategory.name;
            });
            ProductEvent productEvent = selectedCategory == productCategory.name
                ? FilterProductByCategory(productCategory: productCategory.name)
                : LoadProduct();
            BlocProvider.of<ProductBloc>(context).add(productEvent);
          },
        );
      },
    );
  }
}
