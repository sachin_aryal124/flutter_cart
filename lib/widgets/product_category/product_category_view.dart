import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/product_category/product_category.dart';
import '../views/loading_view.dart';
import 'product_category_list.dart';

const double productCategoryViewHeight = 60;

class ProductCategoryView extends StatelessWidget {
  const ProductCategoryView({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductCategoryBloc, ProductCategoryState>(
      builder: (context, state) {
        if (state is ProductCategoryLoading) {
          return const SizedBox(
            height: productCategoryViewHeight,
            child: LoadingView(),
          );
        }
        if (state is ProductCategoryError) {
          return SizedBox(
            height: productCategoryViewHeight,
            child: Text(state.message),
          );
        }
        if (state is ProductCategoryLoaded) {
          return SizedBox(
            height: productCategoryViewHeight,
            child: ProductCategoryList(
              productCategories: state.productCategories,
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}
