import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/cart/cart.dart';
import '../../screens/cart_screen.dart';

class CartQuantityBadge extends StatelessWidget {
  const CartQuantityBadge({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartBloc, CartState>(
      builder: (context, state) {
        int totalCartItems = state is CartLoaded
            ? state.carts.fold(
                0, (previousValue, element) => previousValue + element.quantity)
            : 0;
        return Badge(
          alignment: AlignmentDirectional.topEnd,
          label: Text(totalCartItems.toString()),
          child: IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed(
                CartScreen.routeName,
              );
            },
            icon: const Icon(Icons.shopping_cart),
          ),
        );
      },
    );
  }
}
