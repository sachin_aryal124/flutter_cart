import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/cart/cart.dart';
import '../../models/cart/cart.dart';
import '../../utils/currency.dart';
import 'cart_quantity_button_group.dart';

class CartItem extends StatelessWidget {
  final Cart cart;
  const CartItem({
    super.key,
    required this.cart,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.network(
                cart.product.image,
                width: 100,
                height: 100,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(width: 16.0),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    cart.product.title,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  const SizedBox(height: 16.0),
                  Row(
                    children: [
                      Text(
                        formatCurrency(cart.product.price),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium
                            ?.copyWith(color: Theme.of(context).primaryColor),
                      ),
                      const Spacer(),
                      CartQuantityButtonGroup(
                        onIncrementTap: () {
                          BlocProvider.of<CartBloc>(context).add(
                            UpdateCartQuantity(
                              cart: cart,
                              action: UpdateCartQuantityAction.increment,
                            ),
                          );
                        },
                        onDecrementTap: () {
                          BlocProvider.of<CartBloc>(context).add(
                            UpdateCartQuantity(
                              cart: cart,
                              action: UpdateCartQuantityAction.decrement,
                            ),
                          );
                        },
                        quantity: cart.quantity,
                      )
                    ],
                  ),
                  const Divider(height: 32.0),
                  Row(
                    children: [
                      const Text('Total Price'),
                      const Spacer(),
                      Text(
                        formatCurrency(cart.quantity * cart.product.price),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium
                            ?.copyWith(color: Theme.of(context).primaryColor),
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
