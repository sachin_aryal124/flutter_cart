import 'package:flutter/material.dart';

class CartQuantityButtonGroup extends StatelessWidget {
  final VoidCallback onIncrementTap;
  final VoidCallback onDecrementTap;
  final int quantity;
  const CartQuantityButtonGroup({
    super.key,
    required this.onIncrementTap,
    required this.onDecrementTap,
    required this.quantity,
  });
  @override
  Widget build(BuildContext context) {
    BorderRadius iconButtonBorderRadius = BorderRadius.circular(50.0);
    EdgeInsetsGeometry iconButtonPadding = const EdgeInsets.symmetric(
      horizontal: 12.0,
      vertical: 6.0,
    );
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50.0),
        border: Border.all(
          color: Theme.of(context).hintColor,
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          InkWell(
            borderRadius: iconButtonBorderRadius,
            onTap: onIncrementTap,
            child: Ink(
              padding: iconButtonPadding,
              child: const Icon(Icons.add),
            ),
          ),
          Text(quantity.toString()),
          InkWell(
            borderRadius: iconButtonBorderRadius,
            onTap: onDecrementTap,
            child: Ink(
              padding: iconButtonPadding,
              child: const Icon(Icons.remove),
            ),
          ),
        ],
      ),
    );
  }
}
