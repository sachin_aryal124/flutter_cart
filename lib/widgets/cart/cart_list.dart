import 'package:flutter/material.dart';
import 'package:flutter_cart/widgets/cart/cart_item.dart';

import '../../models/cart/cart.dart';

class CartList extends StatelessWidget {
  final List<Cart> cartItems;
  const CartList({
    super.key,
    required this.cartItems,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: cartItems.length,
      itemBuilder: (context, index) {
        Cart cart = cartItems[index];
        return CartItem(cart: cart);
      },
    );
  }
}
