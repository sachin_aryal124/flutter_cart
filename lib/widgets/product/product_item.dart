import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cart/blocs/product/product.dart';
import 'package:uuid/uuid.dart';

import '../../blocs/cart/cart.dart';
import '../../models/cart/cart.dart';
import '../../models/product/product.dart';
import '../../utils/currency.dart';
import 'product_rating.dart';

class ProductItem extends StatelessWidget {
  final Product product;
  const ProductItem({
    super.key,
    required this.product,
  });

  @override
  Widget build(BuildContext context) {
    void handleAddToCart() {
      Uuid uuid = const Uuid();
      Cart cart = Cart(
        id: uuid.v4(),
        product: product,
      );
      BlocProvider.of<CartBloc>(context).add(
        AddToCart(
          cart: cart,
        ),
      );
    }

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.network(
                product.image,
                width: 100,
                height: 100,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(width: 16.0),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    product.title,
                    textAlign: TextAlign.left,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  const SizedBox(height: 8.0),
                  Text(
                    product.description,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context)
                        .textTheme
                        .bodySmall
                        ?.copyWith(height: 1.5),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ProductRating(product: product),
                      const Spacer(),
                      Text(
                        formatCurrency(product.price),
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                      IconButton(
                        onPressed: handleAddToCart,
                        icon: const Icon(
                          Icons.shopping_cart,
                        ),
                      )
                    ],
                  ),
                  const Divider(),
                  OutlinedButton(
                    style: ElevatedButton.styleFrom(
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(50.0),
                        ),
                      ),
                    ),
                    onPressed: () {
                      BlocProvider.of<ProductBloc>(context).add(
                        FilterProductByCategory(
                          productCategory: product.category,
                        ),
                      );
                    },
                    child: Text(
                      product.category,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
