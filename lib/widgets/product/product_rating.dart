import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../models/product/product.dart';

class ProductRating extends StatelessWidget {
  final Product product;
  const ProductRating({
    super.key,
    required this.product,
  });

  @override
  Widget build(BuildContext context) {
    return RatingBar.builder(
      glow: false,
      itemSize: 20.0,
      initialRating: product.rating.rate,
      itemCount: 5,
      allowHalfRating: true,
      itemBuilder: (context, index) {
        return const Icon(
          Icons.star,
          color: Colors.amber,
        );
      },
      onRatingUpdate: (value) {},
    );
  }
}
