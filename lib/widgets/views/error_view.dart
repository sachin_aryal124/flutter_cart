import 'package:flutter/widgets.dart';

class ErrorView extends StatelessWidget {
  final String message;
  const ErrorView({
    super.key,
    required this.message,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(message),
    );
  }
}

class ExpandedErrorView extends StatelessWidget {
  final String message;
  const ExpandedErrorView({
    super.key,
    required this.message,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ErrorView(message: message),
    );
  }
}
