import 'package:flutter/material.dart';

class LoadingView extends StatelessWidget {
  const LoadingView({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}

class ExpandedLoadingView extends StatelessWidget {
  const ExpandedLoadingView({super.key});

  @override
  Widget build(BuildContext context) {
    return const Expanded(
      child: LoadingView(),
    );
  }
}
